<?php
session_start();

$host = "localhost";
$user = "root";
$password = "eugene";
$database = "test";
//create table webtest (id int primary key auto_increment, fruit char(20), color char(20));


// initialize PDO
$dbConnection = new PDO("mysql:host=$host;dbname=$database;charset=utf8",$user,$password);
$dbConnection->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
$dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

if( isset($_POST['fruit']) && isset($_POST['color']) ){
	$request = $dbConnection -> prepare("insert into webtest(fruit,color) values(?,?);");
	$request -> execute( array($_POST['fruit'],$_POST['color']) );
	$id = $dbConnection->lastInsertId();
}

$request = $dbConnection->query('select * from webtest;');
$request -> setFetchMode(PDO::FETCH_ASSOC);
$data = $request->fetchAll();

?>
<html>
<head>
	<title>PHP Demo 3</title>
	<style>
		table {
			border: 1px solid black;
		}
	</style>
</head>

<body>
	<h2>Data from DB</h2>
	<table>
		<tr>
			<th>Fruit</th>
			<th>Color</th>
		</tr>
		<?php
			foreach( $data as $key => $value )
			{
				echo "<tr>\n";
				echo "<td>".$value['fruit']."</td>\n";
				echo "<td>".$value['color']."</td>\n";
				echo "</tr>\n";
			}
			
		?>
	</table>
	<h3>Insert id is <?php echo $id ?></h3>
	<form method="POST" action="php_example_3.php" >
		<p>Fruit: <input name="fruit"></p>
		<p>Color: <input name="color"></p>
		<input type="submit">
	</form>
</body>

</html>




