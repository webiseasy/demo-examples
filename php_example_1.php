<?php

$numarr = array('Andy','Mary','NTHU');
$assarr = array('apple'=>'red','banana'=>'yellow','orange'=>'orange');


?>
<html>
<head>
	<title>PHP Demo </title>
	<style>
		table {
			border: 1px solid black;
		}
	</style>
</head>

<body>
	<table>
		<tr>
			<th>Strings??</th>
		</tr>
		<?php
			for($i=0;$i<count($numarr);$i++)
			{
				echo "<tr><td>\n";
				echo $numarr[$i]."\n";
				echo "</td></tr>\n";
			}
		?>
	</table>
	<br>
	<table>
		<tr>
			<th>Fruit</th>
			<th>Color</th>
		</tr>
		<?php
			foreach( $assarr as $key => $value )
			{
				echo "<tr>\n";
				echo "<td>".$key."</td>\n";
				echo "<td>".$value."</td>\n";
				echo "</tr>\n";
			}
			
			for(reset($assarr);$key=key($assarr);next($assarr))
			{
				echo "<tr>\n";
				echo "<td>".$key."</td>\n";
				echo "<td>".$assarr[$key]."</td>\n";
				echo "</tr>\n";
			}
			
		?>
	</table>
</body>

</html>
