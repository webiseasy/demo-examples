SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- 
-- 資料表格式： `singer`
-- 

DROP TABLE IF EXISTS `singer`;
CREATE TABLE `singer` (
  `singerId` int(11) Primary Key AUTO_INCREMENT,
  `singerName` varchar(16) NOT NULL,
  `singerType` varchar(16) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- 
-- 列出以下資料庫的數據： `singer`
-- 

INSERT INTO `singer` VALUES (1, '梁靜茹', '女歌手');
INSERT INTO `singer` VALUES (2, '伍佰', '男歌手');
INSERT INTO `singer` VALUES (3, '五月天', '團體');
INSERT INTO `singer` VALUES (4, '初音未來', '軟體');
INSERT INTO `singer` VALUES (5, '鏡音鈴', '軟體');
INSERT INTO `singer` VALUES (6, '林俊傑', '男歌手');
INSERT INTO `singer` VALUES (7, '朱俐靜', '女歌手');
INSERT INTO `singer` VALUES (8, '蕭敬騰', '男歌手');
INSERT INTO `singer` VALUES (9, '丁噹', '女歌手');

-- ---------------------------------------------------------

-- 
-- 資料表格式： `song`
-- 

DROP TABLE IF EXISTS `song`;
CREATE TABLE `song` (
  `songId` int(11) Primary Key AUTO_INCREMENT,
  `songName` varchar(32) NOT NULL,
  `singerId` int(11) NOT NULL,
  `songLang` varchar(16) NOT NULL,
  `youtube` varchar(256) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- 
-- 列出以下資料庫的數據： `song`
-- 

INSERT INTO `song` VALUES (1,'會呼吸的痛', 1, '華語', 'http://www.youtube.com/watch?v=SZnmpF9K_VQ');
INSERT INTO `song` VALUES (2,'乾杯', 3, '華語', 'http://www.youtube.com/watch?v=qX2GsMj7154');
INSERT INTO `song` VALUES (3,'溫柔', 3, '華語', 'http://www.youtube.com/watch?v=nWb_X3ZJQjw');
INSERT INTO `song` VALUES (4,'OK啦', 3, '台語', 'http://www.youtube.com/watch?v=_FV1NDdHHEo');
INSERT INTO `song` VALUES (5,'愛情限時批', 2, '台語', 'http://www.youtube.com/watch?v=b5Qrin1tRDk');
INSERT INTO `song` VALUES (6,'夢醒時分', 2, '華語', 'http://www.youtube.com/watch?v=m9Y5z5sYm3s');
INSERT INTO `song` VALUES (7,'夢醒時分', 1, '華語', 'http://www.youtube.com/watch?v=JbvnzMZnEfc');
INSERT INTO `song` VALUES (8,'甩蔥歌', 4, '日語', 'http://www.youtube.com/watch?v=QwZ_VKBTjWo');
INSERT INTO `song` VALUES (9,'千本櫻', 4, '日語', 'http://www.youtube.com/watch?v=2LUgH_X7sFM');
INSERT INTO `song` VALUES (10,'修煉愛情', 4, '華語', 'https://www.youtube.com/watch?v=LWV-f6dMN3Q');
INSERT INTO `song` VALUES (11,'存在的力量', 4, '華語', 'https://www.youtube.com/watch?v=4Ubok_b1flk');
INSERT INTO `song` VALUES (12,'Marry me', 4, '華語', 'https://www.youtube.com/watch?v=HFnugU9kKlg');
INSERT INTO `song` VALUES (13,'好難得', 4, '華語', 'https://www.youtube.com/watch?v=8yvWJ6nq_HE');
INSERT INTO `song` VALUES (14,'我會在你身邊', 4, '華語', 'https://www.youtube.com/watch?v=Wo_KGzyV9w4');
INSERT INTO `song` VALUES (15,'飛機', 4, '華語', 'https://www.youtube.com/watch?v=uYonUSAYKc0');
INSERT INTO `song` VALUES (16,'只能想念你', 4, '華語', 'https://www.youtube.com/watch?v=VEYVQmgWsIg');

-- --------------------------------------------------------

-- 
-- 資料表格式： `songrank`
-- 

DROP TABLE IF EXISTS `songrank`;
CREATE TABLE `songrank` (
  `ssn` int(11) Primary key auto_increment,
  `thisRank` int(11) NOT NULL,
  `prevRank` int(11) NOT NULL,
  `songId` int(11) NOT NULL,
  `singerId` int(11) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

-- 
-- 列出以下資料庫的數據： `songrank`
-- 

INSERT INTO `songrank` VALUES (1, 1, 1, 10, 6);
INSERT INTO `songrank` VALUES (2, 11, 12, 11, 7);
INSERT INTO `songrank` VALUES (3, 17, 17, 12, 8);
INSERT INTO `songrank` VALUES (4, 46, 42, 13, 9);
INSERT INTO `songrank` VALUES (5, 60, 57, 14, 7);
INSERT INTO `songrank` VALUES (6, 63, 64, 15, 6);
INSERT INTO `songrank` VALUES (7, 98, 99, 16, 8);