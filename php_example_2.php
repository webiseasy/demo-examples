<?php
session_start();

$assarr = array('apple'=>'red','banana'=>'yellow','orange'=>'orange');

if( $_GET['reset'] == 1 || !isset($_SESSION['arr']) )
	$_SESSION['arr'] = $assarr;

if( isset($_POST['fruit']) && isset($_POST['color']) ){
	$_SESSION['arr'][ $_POST['fruit'] ] = $_POST['color'];
	$assarr[ $_POST['fruit'] ] = $_POST['color'];
}




?>
<html>
<head>
	<title>PHP Demo 2</title>
	<style>
		table {
			border: 1px solid black;
		}
	</style>
</head>

<body>
	<h2>Content of $assarr</h2>
	<table>
		<tr>
			<th>Fruit</th>
			<th>Color</th>
		</tr>
		<?php
			foreach( $assarr as $key => $value )
			{
				echo "<tr>\n";
				echo "<td>".$key."</td>\n";
				echo "<td>".$value."</td>\n";
				echo "</tr>\n";
			}
			
		?>
	</table>
	<h2>Content of Session</h2>
	<table>
		<tr>
			<th>Fruit</th>
			<th>Color</th>
		</tr>
		<?php
			foreach( $_SESSION['arr'] as $key => $value )
			{
				echo "<tr>\n";
				echo "<td>".$key."</td>\n";
				echo "<td>".$value."</td>\n";
				echo "</tr>\n";
			}
			
		?>
	</table>
	<form method="POST" action="php_example_2.php" >
		<p>Fruit: <input name="fruit"></p>
		<p>Color: <input name="color"></p>
		<input type="submit">
	</form>
</body>

</html>
